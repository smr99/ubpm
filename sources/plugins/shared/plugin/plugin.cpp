#include "plugin.h"

DEVICEINFO DevicePlugin::getDeviceInfo()
{
	QFile png(IMAGE), pdf(MANUAL);

	png.open(QIODevice::ReadOnly);
	pdf.open(QIODevice::ReadOnly);

	QByteArray PNG = png.readAll();
	QByteArray PDF = pdf.readAll();

	png.close();
	pdf.close();

	return DEVICEINFO { PRODUCER, MODEL, ALIAS, MAINTAINER, VERSION, ICON, PNG, PDF };
}

bool DevicePlugin::getDeviceData(QWidget *parent, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2, struct SETTINGS *settings)
{
	DialogImport dlg(parent, theme, user1, user2, settings);

	if(!dlg.failed)
	{
		return dlg.exec();
	}

	return QDialog::Rejected;
}
