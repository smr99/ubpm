<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Importation depuis l&apos;Appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="84"/>
        <source>Plugin Options</source>
        <translation>Options du Plugin</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="24"/>
        <source>Bluetooth Controller</source>
        <translation>Contrôleur Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="110"/>
        <source>Import Measurements automatically</source>
        <translation>Importer Automatiquement les Mesures</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="30"/>
        <source>Select Bluetooth Controller</source>
        <translation>Sélectionner le Contrôleur Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="37"/>
        <source>Discover Device</source>
        <translation>Découverte de l&apos;Appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="120"/>
        <source>Device Information</source>
        <translation>Information sur l&apos;Appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="51"/>
        <source>Bluetooth Device</source>
        <translation>Appareil Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="60"/>
        <source>Select Bluetooth Device</source>
        <translation>Sélectionner l&apos;Appareil Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="69"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="69"/>
        <source>Serial</source>
        <translation>Port Série</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="70"/>
        <source>Connect Device</source>
        <translation>Connecter l&apos;Appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="156"/>
        <source>Producer</source>
        <translation>Fabricant</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="163"/>
        <source>Product</source>
        <translation>Produit</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="86"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="90"/>
        <source>Discover Device automatically</source>
        <translation>Découverte Automatique de l&apos;Appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="103"/>
        <source>Connect Device automatically</source>
        <translation>Connecter automatique l&apos;Appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="158"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="182"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="111"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="188"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="173"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="132"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="262"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="199"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="128"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="205"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="190"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="149"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="279"/>
        <source>Write Logfile</source>
        <translation>Écrire un Fichier de Log</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="149"/>
        <source>Firmware</source>
        <translation>Micrologiciel</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="225"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="154"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="178"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="231"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="178"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="216"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="305"/>
        <source>Cancel</source>
        <translation>Annulé</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="24"/>
        <source>Could not find a serial port.

Is the usb2serial driver installed and the device connected?</source>
        <translation>Ne peu pas trouver le port serie.

Le pilote usb2serial est-il installé et l&apos;appareil est-il connecté ?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="76"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="76"/>
        <source>Could not read data.

%1</source>
        <translation>Impossible de lire les données.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="212"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="253"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="203"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="240"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="382"/>
        <source>The import was canceled.</source>
        <translation>L&apos;importation a été annulée.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="75"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="159"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="112"/>
        <source>Could not write data.

%1</source>
        <translation>Impossible d&apos;écrire des données.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.

Press &quot;MEM&quot;, select U1/U2 with &quot;POWER&quot; and wait until the user data is shown.

Then try again…</source>
        <translation>L&apos;appareil ne répond pas.

Appuyez sur &quot;MEM&quot;, sélectionnez U1/U2 avec &quot;POWER&quot; et attendez que les données de l&apos;utilisateur soient affichées.

Alors essayez à nouveau…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="115"/>
        <source>Was &quot;U1&quot; selected on the device?

For &quot;U2&quot; answer with No.</source>
        <translation>L&apos; &quot;U1&quot; a-t-elle été sélectionnée sur l&apos;appareil ?

Pour &quot;U2&quot;, répondre par Non.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="180"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="200"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="173"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="198"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="252"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="187"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="273"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="177"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="659"/>
        <source>Could not open the logfile %1.

%2</source>
        <translation>Impossible d&apos;ouvrir le fichier journal %1.

%2</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="316"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="270"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="318"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="222"/>
        <source>Could not open serial port &quot;%1&quot;.

%2</source>
        <translation>Impossible d&apos;ouvrir le port série &quot;%1&quot;.

%2</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="190"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="183"/>
        <source>The device doesn&apos;t respond.

Disconnect the USB cable and reconnect it.

Then try again…</source>
        <translation>L&apos;appareil ne répond pas.

Débranchez le câble USB et rebranchez-le.

Essayez à nouveau…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="242"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="192"/>
        <source>Could not read measurement count.</source>
        <translation>Impossible de lire le nombre de mesures.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="223"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="264"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="214"/>
        <source>Could not read measurement %1.</source>
        <translation>Impossible de lire la mesure %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="330"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="284"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="682"/>
        <source>Cancel import?</source>
        <translation>Annuler import ?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="297"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="341"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="295"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="343"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="482"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="693"/>
        <source>Import in progress…</source>
        <translation>Importation des données en progression…</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="262"/>
        <source>No answer from the device.

Did you use the Bluetooth controller with the cloned MAC address?</source>
        <translation>Pas de réponse de l&apos;Appareil.

Avez-vous utilisé le contrôleur Bluetooth avec l&apos;adresse MAC clonée ?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="280"/>
        <source>Can&apos;t set device into info mode.</source>
        <translation>Impossible de mettre l&apos;appareil en mode info.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="293"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="307"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="349"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="366"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="403"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="442"/>
        <source>Send %1 command failed.</source>
        <translation>L&apos;envoi de la commande %1 a échoué.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="319"/>
        <source>Can&apos;t set device into data mode.</source>
        <translation>Impossible de mettre l&apos;appareil en mode donnés.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="70"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="387"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="429"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="202"/>
        <source>Import aborted by user.</source>
        <translation>importation interrompue par l&apos;utilisateur.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="152"/>
        <source>Acknowledge failed.</source>
        <translation>Échec de l&apos;acquittement.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="84"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="100"/>
        <source>Could not read data.

Is the device powered on?

%1</source>
        <translation>Impossible de lire les données.

L&apos;appareil est-il sous tension ?

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="45"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="40"/>
        <source>Could not open usb device %1:%2.</source>
        <translation>Impossible d&apos;ouvrir le périphérique usb %1:%2.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="58"/>
        <source>No Bluetooth controller found.</source>
        <translation>Aucun contrôleur Bluetooth trouvé.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="107"/>
        <source>Bluetooth error.

%1</source>
        <translation>Erreur Bluetooth.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="122"/>
        <source>No device discovered.

Check the connection in operating system or press Bluetooth button on device and try again…</source>
        <translation>Aucun appareil n&apos;a été découvert.

Vérifiez la connexion dans le système d&apos;exploitation ou appuyez sur le bouton Bluetooth de l&apos;appareil et réessayez…</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="154"/>
        <source>The selected device is not a %1.</source>
        <translation>Le dispositif sélectionné n&apos;est pas un %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="163"/>
        <source>Device doesn&apos;t respond, try again?</source>
        <translation>L&apos;Appareil ne répond pas, réessayez ?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="177"/>
        <source>Could not connect to the device.

Check the connection in operating system or press Bluetooth button on device and try again…

%1</source>
        <translation>Impossible de se connecter à l&apos;appareil.

Vérifiez la connexion dans le système d&apos;exploitation ou appuyez sur le bouton Bluetooth de l&apos;appareil et rééssayez...

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="187"/>
        <source>Press START/STOP on device and try again…</source>
        <translation>Appuyez sur la touche START/STOP de l&apos;appareil et réessayez…</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="471"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="233"/>
        <source>Really abort import?</source>
        <translation>L&apos;importation est vraiment interrompue ?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="431"/>
        <source>Could not access the Bluetooth service %1.</source>
        <translation>n&apos;a pas pu accéder au service Bluetooth %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="607"/>
        <source>The selected Bluetooth controller is not available.</source>
        <translation>Le contrôleur Bluetooth sélectionné n&apos;est pas disponible.</translation>
    </message>
</context>
</TS>
