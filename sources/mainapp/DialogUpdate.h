#ifndef DLGUPDATE_H
#define DLGUPDATE_H

#include "MainWindow.h"
#include "ui_DialogUpdate.h"

#ifdef Q_OS_LINUX
	#define IMG ":/svg/update_lin.svg"
	#define BIN ".AppImage"
#elif defined Q_OS_WIN
	#define IMG ":/svg/update_win.svg"
	#define BIN ".exe"
#elif defined Q_OS_MACOS
	#define IMG ":/svg/update_mac.svg"
	#define BIN ".dmg"
#endif

#define RELEASES QUrl("https://codeberg.org/api/v1/repos/lazyt/ubpm/releases?pre-release=false&limit=1")
#define TIMEOUT 5000

class DialogUpdate : public QDialog, private Ui::DialogUpdate
{
	Q_OBJECT

public:

	explicit DialogUpdate(QWidget*, bool);

private:

	QNetworkAccessManager *nam;
	QNetworkReply *nr = nullptr;
	QWidget *parent;
	bool notification;
	int size;
	QString url, name;

	void checkRelease(QByteArray);
	void saveDownload(QByteArray);

private slots:

	void sslErrors(QNetworkReply*, const QList<QSslError>&);
	void finished(QNetworkReply*);
	void downloadProgress(qint64, qint64);

	void on_pushButton_update_clicked();
	void on_pushButton_ignore_clicked();

	void keyPressEvent(QKeyEvent*);

	void reject();
};

#endif // DLGUPDATE_H
