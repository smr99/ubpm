<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>DialogAnalysis</name>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Result</numerusform>
            <numerusform>Results</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Match</numerusform>
            <numerusform>Matches</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Record</numerusform>
            <numerusform>Records</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogDistribution</name>
    <message numerus="yes">
        <location filename="../DialogDistribution.cpp" line="18"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Record</numerusform>
            <numerusform>Records</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="747"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation>
            <numerusform>Successfully migrated %n record for user %1.</numerusform>
            <numerusform>Successfully migrated %n records for user %1.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="751"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Skipped %n invalid record!</numerusform>
            <numerusform>Skipped %n invalid records!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="756"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Skipped %n duplicate record!</numerusform>
            <numerusform>Skipped %n duplicate records!</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem:

%1
Do you wish to continue anyway?</numerusform>
            <numerusform>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problems:

%1
Do you wish to continue anyway?</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2697"/>
        <location filename="../MainWindow.cpp" line="3930"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Successfully imported %n record from %1.

     User 1 : %2
     User 2 : %3</numerusform>
            <numerusform>Successfully imported %n records from %1.

     User 1 : %2
     User 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2701"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Skipped %n invalid record!</numerusform>
            <numerusform>Skipped %n invalid records!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2706"/>
        <location filename="../MainWindow.cpp" line="3934"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Skipped %n duplicate record!</numerusform>
            <numerusform>Skipped %n duplicate records!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="4359"/>
        <source>The following %n translation(s) for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</source>
        <translation>
            <numerusform>The following %n translation for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</numerusform>
            <numerusform>The following %n translations for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</numerusform>
        </translation>
    </message>
</context>
</TS>
