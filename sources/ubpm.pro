TEMPLATE = subdirs
SUBDIRS  = plugins mainapp

mainapp.depends = plugins

appbundle.target  = appbundle
appbundle.CONFIG  = recursive
appbundle.recurse = mainapp plugins
appbundle.recurse_target = appbundle

installer.target  = installer
installer.CONFIG  = recursive
installer.recurse = mainapp plugins
installer.recurse_target = installer

QMAKE_EXTRA_TARGETS += appbundle installer
